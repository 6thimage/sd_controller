`timescale 1ns / 1ps

module sclk_controller_tb;

	// Inputs
	reg clk;
	reg reset;
	wire clock_strobe;
	reg enable;

	// Outputs
	wire sclk;
    wire last_clocks;

	// Instantiate the Unit Under Test (UUT)
	sclk_controller uut (
		.clk(clk), 
		.reset(reset), 
		.clock_strobe(clock_strobe), 
		.enable(enable), 
		.sclk(sclk),
        .last_clocks(last_clocks)
	);

    always #0.5 clk <= ~clk;

    mod_m_counter #(.m(2)) strobe(.clk(clk), .reset(1'b1), .enable(1'b1), .hit(clock_strobe));

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		enable = 0;

        #1;
        /* remove from reset */
        reset=1;
        
        #1;
        /* enable */
        enable=1;
        
        #2;
        /* disable */
        enable=0;
        
        #40;
        $finish;

	end
      
endmodule

