#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libftdi1/ftdi.h>

int close_signal=0;

void signal_handler(int i)
{
    close_signal=1;
    puts("\nSignal caught - closing");
}

int main(int argc, char *argv[])
{
    struct ftdi_context *ftdi;
    struct ftdi_device_list *device_list;
    int i, devices, ret, opt;
    unsigned int max_count=0, bytes_to_skip=0;
    char *filename=0;
    FILE *f;

    /* process arguments */
    while((opt=getopt(argc, argv, "?s:n:"))!=-1)
    {
        switch(opt)
        {
        case 's':
            bytes_to_skip=atoi(optarg);
            break;
        case 'n':
            max_count=atoi(optarg);
            break;
        case '?':
        default:
            printf("Usage: %s [-s bytes_to_skip] [-n count] output_file\n", argv[0]);
            return -1;
        }
    }

    if(optind>=argc || !strlen(argv[optind]))
    {
        puts("File name required");
        return -1;
    }
    filename=malloc(strlen(argv[optind])+1);
    if(!filename)
    {
        printf("Failed to malloc: filename %zd\n", strlen(optarg)+1);
        return -1;
    }
    strcpy(filename, argv[optind]);

    /* open file */
    f=fopen(filename, "wb");
    if(!f)
    {
        printf("Failed to open file '%s'\n", filename);
        return -1;
    }

    /* create context */
    ftdi=ftdi_new();
    if(!ftdi)
    {
        printf("Failed to create context\n");
        return -1;
    }

    /* print library version */
    printf("libftdi %s\n\n", ftdi_get_library_version().version_str);

    /* search for devices */
    devices=ftdi_usb_find_all(ftdi, &device_list, 0, 0);
    if(devices<0)
    {
        printf("Failed to search for devices (%d)\n", devices);
        goto error_exit;
    }
    printf("Found %d devices\n", devices);

    /* iterate over devices */
    for(i=0; i<devices; ++i)
    {
        int j;
        char man[100], desc[100], serial[100];
        j=ftdi_usb_get_strings(ftdi, device_list[i].dev, man, 100, desc, 100, serial, 100);
        if(!j)
            printf("% 2d - %s %s %s\n", i, man, desc, serial);
        else
            printf("% 2d - List failed (%d)\n", i, j);
    }

    /* if only one device open it */
    if(devices==1)
    {
        ftdi_set_interface(ftdi, INTERFACE_B);
        ret=ftdi_usb_open_dev(ftdi, device_list[0].dev);
        if(ret<0)
        {
            printf("Failed to open device (%d)\n", ret);
            /* free list */
            ftdi_list_free(&device_list);
            goto error_exit;
        }
    }
    else if(devices>1)
    {
        printf("Too many devices detected\n");
        /* free list */
        ftdi_list_free(&device_list);
        goto exit;
    }
    else
    {
        /* free list */
        ftdi_list_free(&device_list);
        goto exit;
    }

    /* set fifo mode */
    ret=ftdi_set_bitmode(ftdi, 0xff, BITMODE_SYNCFF);
    if(ret<0)
    {
        printf("Failed to set mode (%d)\n", ret);
        goto close_exit;
    }

    /* set signal handler */
    signal(SIGINT, signal_handler);

    /* read in data */
    while(!close_signal)
    {
        static unsigned long counter=0;
        unsigned char buffer;

        ret=ftdi_read_data(ftdi, &buffer, 1);
        if(ret<0)
        {
            printf("Bad read (%d)\n", ret);
            break;
        }
        if(ret)
        {
            if(bytes_to_skip)
            {
                printf("%6ld 0x%02x - ", ++counter, buffer&0xff);
                for(i=0; i<4; ++i)
                    printf("%d", (buffer>>(7-i))&1);
                printf(" ");
                for(; i<8; ++i)
                    printf("%d", (buffer>>(7-i))&1);
                puts("");

                --bytes_to_skip;
            }
            else if(fwrite(&buffer, 1, 1, f)!=1)
            {
                puts("Failed to write data");
                break;
            }
        }
        if(max_count)
            if(counter>=max_count)
                break;
    }

close_exit:
    ret=ftdi_usb_close(ftdi);
    if(ret<0)
        printf("Failed to close device (%d)\n", ret);
exit:
    ftdi_free(ftdi);
    return 0;

error_exit:
    ftdi_free(ftdi);
    return -1;
}

