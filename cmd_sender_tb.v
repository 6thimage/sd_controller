`timescale 1ns / 1ps

module cmd_sender_tb;

	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [7:0] data;
	wire clock_strobe;
    wire sclk;

	// Outputs
	wire cmd_line;
    wire busy;

	// Instantiate the Unit Under Test (UUT)
	cmd_sender uut (
		.clk(clk),
		.reset(reset),
		.enable(enable),
		.data(data),
		.clock_strobe(clock_strobe),
		.sclk(sclk),
		.cmd_line(cmd_line),
        .busy(busy)
	);

    always #0.5 clk <= ~clk;

    clk_strobe strobe(.clk(clk), .reset(1'b1), .clk_div(8'd2), .clk_strobe(clock_strobe));
    wire last_clocks;
    sclk_controller sclk_control(.clk(clk), .reset(1'b1), .clock_strobe(clock_strobe), .enable(busy),
                                 .last_clocks_prevent(1'b0), .sclk(sclk), .last_clocks(last_clocks));

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		enable = 0;
		data = 0;

		// Wait 100 ns for global reset to finish
		#10;

		// Add stimulus here
        reset = 1;
        #2;

        data=8'b01001110;
        enable=1;
        #2;
        enable=0;
        #100;

        data=8'b10110001;
        enable=1;
        #2;
        enable=0;
        #100;

        data=8'b01010001;
        enable=1;
        #2;
        enable=0;
        #100;
        $finish;

	end

endmodule

