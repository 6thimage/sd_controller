`timescale 1ns / 1ps

module erase_test(
     input clkin,
    /* sd card */
    output sd_clk,
     inout sd_cmd,
     inout [3:0] sd_dat,
    /* push button */
     input switch,
    /* LEDs */
    output LED1, LED2, LED3, LED4, LED5,
    /* logic analyser */
    output Wing_C0, Wing_C1, Wing_C2, Wing_C3, Wing_C4, Wing_C5, Wing_C6, Wing_C7,
    /* usb fifo */
     inout [7:0] usb_data,
     input usb_rxf, usb_txe,
    output usb_rd, usb_wr, usb_siwua
    );

    wire clk, clk_50mhz, reset_in;
    //wire dcm_valid;
    //dcm_100mhz dcm(.CLK_IN1(clkin), .CLK_OUT1(clk_50mhz), .CLK_OUT2(clk), .CLK_VALID(dcm_valid));
    assign clk=clkin;
    assign clk_50mhz=clk;
    assign reset_in=!switch ;//|| !dcm_valid;

    /* reset controller */
    wire reset;
    reset_controller reset_control(.clk(clk), .reset_in(reset_in), .reset_out(reset));

    /* sd card controller */
    /* control */
    reg data_enable=1'b0;
    reg [31:0] block_address=32'd0;
    reg data_next=1'b0;
    /* outputs */
    wire [3:0] error;
    wire busy;
    wire idle;
    wire data_next_idle;
    wire version_2;
    wire card_hc;
    wire card_uhs;
    wire [119:0] card_csd;
    //wire [31:0] card_block_size;

    sd_controller sdc(.clk(clk), .reset(reset),
                      .sclk(sd_clk), .cmd(sd_cmd), .data(sd_dat), .card_detect(1'b1),
                      .ram_clk(), .ram_enable(),
                          .ram_write_enable(), .ram_address(),
                          .ram_data_in(), .ram_data_out(8'd0),
                      .data_command_enable(data_enable), .write_block(1'b0),
                          .erase_blocks(1'b1), .block_address(block_address),
                          .multiple_blocks(1'b0), .fixed_count(1'b0),
                          .block_count(32'd0), .last_block(1'b0),
                          .data_next(data_next),
                      .error(error), .idle(idle), .data_next_idle(data_next_idle),
                          .card_version_2(version_2), .card_hc(card_hc), .card_uhs(card_uhs),
                          .card_block_size(/*card_block_size*/));

    /* usb output */
    assign usb_siwua=1'b1;
    reg wr_en=1'b0;
    reg [7:0] wr_data;
    wire wr_full;

    ft245_async_fifo #(.read_depth(3), .write_depth(9), .same_clocks(1)) ft245
                      (.D(usb_data), .RXFn(usb_rxf), .TXEn(usb_txe), .RDn(usb_rd), .WRn(usb_wr),
                       .clk_50mhz(clk_50mhz), .rw_clk(clk),
                       .rd_en(1'b0), .rd_data(), .rd_empty(),
                       .wr_en(wr_en), .wr_data(wr_data), .wr_full(wr_full));

    localparam TEST_START=0, TEST_START_COMPLETE=1, TEST_ERASE_START=2, TEST_ERASE_END=3,
               TEST_ERASE_WAIT=4, TEST_END=5;
    reg [2:0] test_state=TEST_START;

    reg [13:0] send_counter='d0;
    reg trigger=1'b0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            test_state <= TEST_START;
            trigger <= 1'b0;
            send_counter <= 'd0;
        end
        else
        begin
            case(test_state)
            TEST_START:
            begin
                /* wait for sd_controller to idle */
                if(idle)
                begin
                    /* move to next state */
                    test_state <= TEST_START_COMPLETE;
                end
            end

            TEST_START_COMPLETE:
            begin
                if(!wr_full)
                begin
                    if(send_counter=='d0)
                    begin
                        /* 1 */
                        wr_data <= {error, idle, version_2, card_hc, card_uhs};
                        wr_en <= 1'b1;
                    end
                    else
                    begin
                        wr_en <= 1'b0;
                        /* move to next state */
                        if(error=='d0)
                            test_state <= TEST_ERASE_START;
                        else
                        begin
                            wr_data <= {4'd0, error};
                            wr_en <= 1'b1;
                            test_state <= TEST_END;
                        end
                    end

                    /* increment send counter */
                    send_counter <= send_counter + 1'b1;
                end
            end

            TEST_ERASE_START:
            begin
                trigger <= 1'b1;

                /* start erase */
                data_enable <= 1'b1;
                block_address <= 32'd0;
                data_next <= 1'b0;

                /* move to next state */
                test_state <= TEST_ERASE_END;
            end

            TEST_ERASE_END:
            begin
                if(!idle && data_enable)
                    data_enable <= 1'b0;
                else if((data_next_idle||idle) && !data_enable)
                begin
                    if(idle)
                        /* TODO: raise error */
                        test_state <= TEST_END;
                    else
                    begin
                        test_state <= TEST_ERASE_WAIT;
                        /* memory capacity is (block_size+1)*512 KB, not *512B
                         * it seems that there is no upper limit on the erase
                         * address, so we can set this all high and it should
                         * erase every card given to it */
                        block_address <= {32{1'b1}};//card_block_size;
                        data_next <= 1'b1;
                    end
                end
            end

            TEST_ERASE_WAIT:
            begin
                if(idle)
                begin
                    if(error!='d0)
                        wr_data <= {4'd1, error};
                    else
                        wr_data <= {4'd2, 4'd0};
                    wr_en <= 1'b1;
                    test_state <= TEST_END;
                end
            end

            TEST_END:
            begin
                wr_en <= 1'b0;
            end
            endcase
        end
    end

    /* LEDs */
    assign LED1=reset; /* green hdmi */
    assign LED2=(error!='d0); /* red hdmi */
    assign LED3=card_hc; /* green sd */
    assign LED4=version_2; /* red sd */
    assign LED5=idle; /* red usb */

    /* logic analyser */
    assign Wing_C0=1'b0;
    assign Wing_C1=sd_clk;
    assign Wing_C2=sd_cmd;
    assign Wing_C3=sd_dat[0];
    assign Wing_C4=data_next;//sd_dat[1];
    assign Wing_C5=idle;//sd_dat[2];
    assign Wing_C6=(error!='d0);//sd_dat[3];
    assign Wing_C7=trigger;

endmodule
