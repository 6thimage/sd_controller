{
    switch($1)
    {
    case 1:
        printf "status:\n\terror: %s\n\tidle: %s\n\tcard version: %s\n\thigh capacity: %s\n\tUHS: %s\n", substr($4,4,1), substr($5,1,1), substr($5,2,1)+1, substr($5,3,1), substr($5,4,1)
        break
    case 2:
        printf "\tstate: %s\n\tinit_state: %s\n", substr($2,3,1), substr($2,4,1)
        break
    # CID
    case 3:
        printf "CID:\n\tManufacturer ID: %s\n", substr($2,3,2)
        break
    case 4:
        printf "\tOEM ID: %c", strtonum($2)
        break
    case 5:
        printf "%c\n", strtonum($2)
        break
    case 6:
        printf "\tProduct name: %c", strtonum($2)
        break
    case 7:
    case 8:
    case 9:
        printf "%c", strtonum($2)
        break
    case 10:
        printf "%c\n", strtonum($2)
        break
    case 11:
        printf "\tProduct rev: %s.%s\n", substr($2,3,1), substr($2,4,1)
        break
    case 12:
        printf "\tProduct serial: "
        serial=lshift(strtonum($2),24)
        break
    case 13:
        serial=or(serial,lshift(strtonum($2),16))
        break
    case 14:
        serial=or(serial,lshift(strtonum($2),8))
        break
    case 15:
        serial=or(serial,strtonum($2))
        printf "%d (0x%x)\n", serial, serial
        break
    case 16:
        year=2000+strtonum($2)
        break
    case 17:
        printf "\tManufacture date: month %d year %d\n", and(strtonum($2),0xf), rshift(and(strtonum($2),0xf0),4)+year
        break
    # RCA
    case 18:
        rca=lshift(strtonum($2),8)
        break
    case 19:
        rca=or(rca, strtonum($2))
        printf "RCA:\t%d (0x%0.4x)\n", rca, rca
        break
    # CSD
    case 20:
        csd=substr($4,2,1)+1
        printf "CSD:\n\tCSD version: %d\n", csd
        break
    case 21:
        printf "\tTAAC: %d\n", strtonum($2)
        break
    case 22:
        printf "\tNSAC: %d\n", strtonum($2)
        break
    case 23:
        printf "\tTransfer speed: %s\n", $2
        break
    case 24:
        printf "\tCard command classes: %s %s", $4, $5
        break
    case 25:
        printf " %s\n\tRead block length: %d\n", $4, and(strtonum($2),0xf)
        break
    case 26:
        printf "\tRead partial blocks: %s\n\tWrite mis-aligned blocks: %s\n\tRead mis-aligned blocks: %s\n\tDSR implemented: %s\n", substr($4,1,1), substr($4,2,1), substr($4,3,1), substr($4,4,1)
        break
    case 31:
        erase_size=or(erase_size,strtonum(substr($4,1,1)))
        printf "\tErase sector size: %d (0x%0.2x)\n", erase_size, erase_size
        protect_size=and(strtonum($2),0x7f)
        printf "\tWrite protect group size: %d (0x%0.2x)\n", protect_size, protect_size
        break
    case 32:
        write_speed=rshift(and(strtonum($2),0x1c),2)
        max_write=lshift(and(strtonum($2),0x3),2)
        printf "\tWrite protect group enable: %s\n\tWrite speed factor: %d\n", substr($4,1,1), write_speed
        break
    case 33:
        max_write=or(max_write,rshift(and(strtonum($2),0xc0),6))
        printf "\tMax write block length: %d\n\tPartial block write: %s\n", max_write, substr($4,3,1)
        break
    case 34:
        file_format=or(lshift(strtonum(substr($5,1,1)),1),strtonum(substr($5,2,1)))
        printf "\tFile format group: %s\n\tCopy flag: %s\n\tPermanent write protect: %s\n\tTemporary write protect: %s\n\tFile format: %d\n", substr($4,1,1), substr($4,2,1), substr($4,3,1), substr($4,4,1), file_format
        break
    }
    
    if(csd==1)
    {
        switch($1)
        {
        case 26:
            size=lshift(and(strtonum($2),0x3),10)
            break
        case 27:
            size=or(size,lshift(strtonum($2),2))
            break
        case 28:
            size=or(size,rshift(and(strtonum($2),0x3),6))
            r_current_min=rshift(and(strtonum($2),0x38),3)
            r_current_max=and(strtonum($2),0x3)
            printf "\tDevice size: %d (0x%0.6x)\n\tMaximum read current (Vmin): %d\n\tMaximum read current (Vmax): %d\n", size, size, r_current_min, r_current_max
            break
        case 29:
            w_current_min=rshift(and(strtonum($2),0xe0),5)
            w_current_max=rshift(and(strtonum($2),0xc0),2)
            size_mul=lshift(and(strtonum($2),0x3),1)
            printf "\tMaximum write current (Vmin): %d\n\tMaximum write current (Vmax): %d\n", w_current_min, w_current_max
            break
        case 30:
            size_mul=or(size_mul,strtonum(substr($4,1,1)))
            erase_size=lshift(and(strtonum($2),0x3f),1)
            printf "\tSize multiplier: %d\n\tErase single block: %s\n", size_mul, substr($4,2,1)
            break
        }
    }
    else if(csd==2)
    {
        switch($1)
        {
        case 27:
            size=lshift(strtonum($2),16)
            break
        case 28:
            size=or(size, lshift(strtonum($2),8))
            break
        case 29:
            size=or(size, strtonum($2))
            printf "\tDevice size: %d (0x%0.6x)\n", size, size
            break
        case 30:
            erase_size=lshift(and(strtonum($2),0x3f),1)
            printf "\tErase single block: %s\n", substr($4,2,1)
            break
        }
    }
}
