`timescale 1ns / 1ps

module sd_test(
     input clkin,
    /* sd card */
    output sd_clk,
     inout sd_cmd,
     inout [3:0] sd_dat,
    /* push button */
     input switch,
    /* LEDs */
    output LED1, LED2, LED3, LED4, LED5,
    /* logic analyser */
    output Wing_C0, Wing_C1, Wing_C2, Wing_C3, Wing_C4, Wing_C5, Wing_C6, Wing_C7,
    /* usb fifo */
     inout [7:0] usb_data,
     input usb_rxf, usb_txe,
    output usb_rd, usb_wr, usb_siwua
    );

    localparam SINGLE_READ=0, SINGLE_WRITE=1, MULTIPLE_READ=2, MULTIPLE_WRITE=3, ERASE=4,
               FIXED_WRITE=5, FIXED_READ=6;
    localparam test=FIXED_READ;

    if(test==SINGLE_READ)
        single_read_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                            .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                            .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                            .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                            .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                            .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                            .usb_wr(usb_wr), .usb_siwua(usb_siwua));
    else if(test==SINGLE_WRITE)
        single_write_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                             .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                             .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                             .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                             .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                             .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                             .usb_wr(usb_wr), .usb_siwua(usb_siwua));
    else if(test==MULTIPLE_READ)
        multiple_read_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                              .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                              .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                              .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                              .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                              .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                              .usb_wr(usb_wr), .usb_siwua(usb_siwua));
   else if(test==MULTIPLE_WRITE)
        multiple_write_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                               .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                               .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                               .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                               .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                               .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                               .usb_wr(usb_wr), .usb_siwua(usb_siwua));
    else if(test==ERASE)
        erase_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                      .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                      .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                      .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                      .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                      .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                      .usb_wr(usb_wr), .usb_siwua(usb_siwua));
   else if(test==FIXED_WRITE)
        write_fixed_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                            .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                            .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                            .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                            .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                            .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                            .usb_wr(usb_wr), .usb_siwua(usb_siwua));
   else if(test==FIXED_READ)
        read_fixed_test tm(.clkin(clkin), .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_dat(sd_dat),
                           .switch(switch), .LED1(LED1), .LED2(LED2), .LED3(LED3), .LED4(LED4),
                           .LED5(LED5),.Wing_C0(Wing_C0), .Wing_C1(Wing_C1), .Wing_C2(Wing_C2),
                           .Wing_C3(Wing_C3), .Wing_C4(Wing_C4), .Wing_C5(Wing_C5),
                           .Wing_C6(Wing_C6), .Wing_C7(Wing_C7), .usb_data(usb_data),
                           .usb_rxf(usb_rxf), .usb_txe(usb_txe), .usb_rd(usb_rd),
                           .usb_wr(usb_wr), .usb_siwua(usb_siwua));

endmodule
