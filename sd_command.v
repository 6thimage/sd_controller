`timescale 1ns / 1ps

module sd_command #(
    parameter response_timeout_len=8
    )
    (
     input                                  clk,
     input                                  reset,
    /* connections to SD card */
     input                                  sclk,
    output                                  sclk_en,
    output reg                              sclk_last_clocks_prevent,
     input                                  cmd_in,
    output                                  cmd_out,
    output                                  cmd_enable,
    /* connections to control logic */
     input                                  enable,
     input                                  clock_strobe,
     input     [5:0]                        command,          /* not internally stored, must be held constant while busy is high */
     input     [31:0]                       argument,         /* not internally stored, must be held constant while busy is high */
     input     [1:0]                        response_type,    /* not internally stored, must be held constant while busy is high */
     input                                  no_response,      /* not internally stored, must be held constant while busy is high */
     input     [response_timeout_len-1:0]   response_timeout, /* not internally stored, must be held constant while busy is high */
    /* outputs */
    output                                  busy,
    output                                  error_crc,
    output                                  error_timeout,
    output     [119:0]                      response
    );

    /* initial outputs */
    initial
    begin
        sclk_last_clocks_prevent=1'b0;
    end

    reg cmd_send_enable=1'b0;

    /* command line mux */
    assign cmd_enable=cmd_send_reset;

    /* command sender */
    reg cmd_send_reset=1'b0;
    reg [7:0] cmd_send_data=8'h0;
    wire cmd_send_busy;
     /* by default, the command send module will add a check into its idle state that returns the command line
      * high - this might cause issues with SD cards (currently un-tested) as during low data that crosses a
      * byte boundary, the module will pulse the command line within less than half a clock, hence we disable
      * it here as the commands alway leave the line high (they all start with 0 and end in 1)
      */
    cmd_sender send(.clk(clk), .reset(cmd_send_reset), .enable(cmd_send_enable),
                    .data(cmd_send_data), .clock_strobe(clock_strobe), .sclk(sclk), .cmd_line(cmd_out), .busy(cmd_send_busy));

    /* command crc 7 */
    reg cmd_crc_reset=1'b0;
    reg cmd_crc_enable=1'b0;
    wire [6:0] cmd_crc_crc;
    crc7 command_crc(.clk(clk), .reset(cmd_crc_reset), .enable(cmd_crc_enable), .data(cmd_send_data), .crc(cmd_crc_crc));

    /* response receiver
     *
     * the data outputs of response receiver (i.e. response, timeout_error, crc_error) are not reset when resp_recv_reset
     * is brought low. This is done purposefully to maintain the data output, to reduce the number of registers required
     * in this module, but is mentioned here as it is not likely to be the expected behaviour of the module
     */
    reg resp_recv_reset=1'b0;
    reg resp_recv_enable=1'b0;
    wire resp_sclk_en, resp_recv_busy;
    resp_recv #(.timeout_size(response_timeout_len))
              recv(.clk(clk), .reset(resp_recv_reset), .enable(resp_recv_enable),
                   .response_type(response_type), .no_response(no_response), .timeout_len(response_timeout),
                   .clock_strobe(clock_strobe), .sclk(sclk), .sclk_en(resp_sclk_en), .cmd_line(cmd_in),
                   .busy(resp_recv_busy), .timeout_error(error_timeout), .crc_error(error_crc), .response(response));

    /* sclk enable output */
    assign sclk_en=(cmd_send_busy||resp_sclk_en)?1'b1:1'b0;

    /* argument byte counter */
    reg [1:0] arg_counter=2'h0;

    /* state machine */
    localparam STATE_IDLE=0, STATE_SEND=1, STATE_RECV=2;
    reg [1:0] state=STATE_IDLE;
    assign busy=(state!=STATE_IDLE);

    localparam SEND_COMMAND=0, SEND_ARG=1, SEND_CRC=2, SEND_COMPLETE=3;
    reg [1:0] send_state=SEND_COMMAND;

    localparam RECV_ENABLE=0, RECV_WAIT=1;
    reg recv_state=RECV_ENABLE;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* resets */
            cmd_send_reset <= 1'b0;
            cmd_crc_reset <= 1'b0;
            resp_recv_reset <= 1'b0;

            /* enables */
            cmd_send_enable <= 1'b0;
            cmd_crc_enable <= 1'b0;
            resp_recv_enable <= 1'b0;

            /* outputs */
            sclk_last_clocks_prevent <= 1'b0;

            /* reset states */
            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    /* move to next state */
                    state <= STATE_SEND;
                    send_state <= SEND_COMMAND;
                end
            end

            STATE_SEND:
            begin
                /* ensure crc enable is only high for one clock */
                if(cmd_crc_enable)
                    cmd_crc_enable <= 1'b0;

                case(send_state)
                SEND_COMMAND:
                begin
                    /* remove cmd_sender and crc from reset */
                    cmd_send_reset <= 1'b1;
                    cmd_crc_reset <= 1'b1;

                    /* send first byte */
                    cmd_send_data <= {1'b0, 1'b1, command}; /* start bit, transmission bit (host to card), command index */
                    cmd_send_enable <= 1'b1;
                    cmd_crc_enable <= 1'b1;

                    /* reset counter */
                    arg_counter <= 2'h0;

                    /* prevent clocks between command bytes */
                    sclk_last_clocks_prevent <= 1'b1;

                    /* move to next state */
                    send_state <= SEND_ARG;
                end

                SEND_ARG:
                begin
                    /* disable enable once busy flag is raised */
                    if(cmd_send_enable && cmd_send_busy)
                        cmd_send_enable <= 1'b0;
                    /* process next byte when both enable and busy are low */
                    else if(!cmd_send_enable && !cmd_send_busy)
                    begin
                        /* select next byte - this is equivalent to
                         *     {cmd_send_data, argument_int} <= {argument_int, 8'h0};
                         * but requires less LUTs and doesn't require a register for the argument
                         */
                        case(arg_counter)
                        0: cmd_send_data <= argument[31:24];
                        1: cmd_send_data <= argument[23:16];
                        2: cmd_send_data <= argument[15:8];
                        3: cmd_send_data <= argument[7:0];
                        endcase

                        /* send byte */
                        cmd_send_enable <= 1'b1;
                        cmd_crc_enable <= 1'b1;

                        /* increment counter */
                        arg_counter <= arg_counter + 1'b1;

                        /* end condition */
                        if(arg_counter==2'd3)
                            send_state <= SEND_CRC;
                    end
                end

                SEND_CRC:
                begin
                    /* disable enable once busy flag is raised */
                    if(cmd_send_enable && cmd_send_busy)
                        cmd_send_enable <= 1'b0;
                    /* send crc when both enable and busy are low */
                    else if(!cmd_send_enable && !cmd_send_busy)
                    begin
                        /* send crc */
                        cmd_send_data <= {cmd_crc_crc, 1'b1};
                        cmd_send_enable <= 1'b1;

                        /* move to next state */
                        send_state <= SEND_COMPLETE;
                    end
                end

                SEND_COMPLETE:
                begin
                    /* disable enable once busy flag is raised */
                    if(cmd_send_enable && cmd_send_busy)
                    begin
                        cmd_send_enable <= 1'b0;

                        /* enable last clocks */
                        sclk_last_clocks_prevent <= 1'b0;
                    end
                    /* wait until both enable and busy are low */
                    else if(!cmd_send_enable && !cmd_send_busy)
                    begin
                        /* place the command send and crc modules into reset */
                        cmd_send_reset <= 1'b0;
                        cmd_crc_reset <= 1'b0;

                        /* move to next state */
                        recv_state <= RECV_ENABLE;
                        state <= STATE_RECV;
                    end
                end

                default: send_state <= SEND_COMMAND;
                endcase
            end

            STATE_RECV:
            begin
                case(recv_state)
                RECV_ENABLE:
                begin
                    /* remove response receiver from reset */
                    resp_recv_reset <= 1'b1;

                    /* no specific set up is required for response, as this is done internally
                     * with resp_recv based on response_type, which is passed to this module
                     */

                    /* enable receiver */
                    resp_recv_enable <= 1'b1;

                    /* nove to next state */
                    recv_state <= RECV_WAIT;
                end

                RECV_WAIT:
                begin
                    /* disable enable, when busy flag is raised */
                    if(resp_recv_enable && resp_recv_busy)
                        resp_recv_enable <= 1'b0;
                    /* wait until busy flag is lowered */
                    else if(!resp_recv_enable && !resp_recv_busy)
                    begin
                        /* place response receiver into reset */
                        resp_recv_reset <= 1'b0;

                        /* move to idle state */
                        state <= STATE_IDLE;
                    end
                end

                default: recv_state <= RECV_ENABLE;
                endcase
            end

            default: state <= STATE_IDLE;
            endcase
        end
    end

endmodule
