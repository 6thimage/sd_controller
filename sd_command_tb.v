`timescale 1ns / 1ps

module sd_command_tb;

	// Inputs
	reg clk;
	reg reset;
    reg cmd_in;
	reg enable;
	reg [5:0] command;
	reg [31:0] argument;
	reg [2:0] response_type;
    reg no_response;
    reg [7:0] response_timeout;

	// Outputs
	wire sclk;
    wire sclk_en;
    wire last_clocks_prevent;
    wire cmd_out;
    wire cmd_enable;
    wire clock_strobe;
    wire busy;
    wire error_crc;
    wire error_timeout;
    wire [135:0] response;

	// Instantiate the Unit Under Test (UUT)
	sd_command #(.response_timeout_len(8)) uut (
		.clk(clk),
		.reset(reset),
		.sclk(sclk),
        .sclk_en(sclk_en),
        .sclk_last_clocks_prevent(last_clocks_prevent),
		.cmd_in(cmd_in),
        .cmd_out(cmd_out),
        .cmd_enable(cmd_enable),
		.enable(enable),
        .clock_strobe(clock_strobe),
		.command(command),
		.argument(argument),
		.response_type(response_type),
        .no_response(no_response),
        .response_timeout(response_timeout),
        .busy(busy),
        .error_crc(error_crc),
        .error_timeout(error_timeout),
        .response(response)
	);

    always #0.5 clk <= ~clk;

    clk_strobe strobe(.clk(clk), .reset(1'b1), .clk_div(8'd0), .clk_strobe(clock_strobe));
    wire last_clocks;
    sclk_controller sclk_control(.clk(clk), .reset(1'b1), .clock_strobe(clock_strobe), .enable(sclk_en),
                                 .last_clocks_prevent(last_clocks_prevent), .sclk(sclk), .last_clocks(last_clocks));

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
        cmd_in = 1;
		enable = 0;
		command = 0;
		argument = 0;
		response_type = 0;
        no_response = 0;
        response_timeout = 8;

		// Wait 100 ns for global reset to finish
		//#100;

		// Add stimulus here
        #2;
        reset=1;

        #2;
        command=17;
        argument=0;
        no_response=0;
        response_type=2'b01;
        enable=1;

        #2;
        enable=0;

        #1000;
        $finish;

	end

endmodule

