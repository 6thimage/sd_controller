`timescale 1ns / 1ps

module crc7_tb;

	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [7:0] data;

	// Outputs
	wire [6:0] crc;

	// Instantiate the Unit Under Test (UUT)
	crc7 uut (
		.clk(clk),
		.reset(reset),
		.enable(enable),
		.data(data),
		.crc(crc)
	);

    always #0.5 clk <= ~clk;
	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		enable = 0;
		data = 0;

		// Wait 100 ns for global reset to finish
		//#100;

		// Add stimulus here
        #2;
        reset=1;

        #1;
        data=8'b01000000;
        enable=1;
        #1;
        enable=0;

        #1;
        data=8'b00000000;
        enable=1;
        #1;
        enable=0;

        #1;
        data=8'b00000000;
        enable=1;
        #1;
        enable=0;

        #1;
        data=8'b00000000;
        enable=1;
        #1;
        enable=0;

        #1;
        data=8'b00000000;
        enable=1;
        #1;
        enable=0;

        if(crc!=7'h4a)
            $display("FAIL - output crc is incorrect - expected 0x4a, got 0x%h", crc);
        else
            $display("PASS");

        $finish;

	end

endmodule

