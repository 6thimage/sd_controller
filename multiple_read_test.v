`timescale 1ns / 1ps

module multiple_read_test(
     input clkin,
    /* sd card */
    output sd_clk,
     inout sd_cmd,
     inout [3:0] sd_dat,
    /* push button */
     input switch,
    /* LEDs */
    output LED1, LED2, LED3, LED4, LED5,
    /* logic analyser */
    output Wing_C0, Wing_C1, Wing_C2, Wing_C3, Wing_C4, Wing_C5, Wing_C6, Wing_C7,
    /* usb fifo */
     inout [7:0] usb_data,
     input usb_rxf, usb_txe,
    output usb_rd, usb_wr, usb_siwua
    );

    wire clk, clk_50mhz, reset_in;
    //wire dcm_valid;
    //dcm_100mhz dcm(.CLK_IN1(clkin), .CLK_OUT1(clk_50mhz), .CLK_OUT2(clk), .CLK_VALID(dcm_valid));
    assign clk=clkin;
    assign clk_50mhz=clk;
    assign reset_in=!switch ;//|| !dcm_valid;

    /* reset controller */
    wire reset;
    reset_controller reset_control(.clk(clk), .reset_in(reset_in), .reset_out(reset));

    /* sd card controller */
    /* ram */
    wire sd_ram_clk;
    wire sd_ram_enable;
    wire sd_ram_write_enable;
    wire [8:0] sd_ram_address;
    wire [7:0] sd_ram_data_in;
    wire [7:0] sd_ram_data_out;
    /* control */
    reg data_enable=1'b0;
    reg data_write_block=1'b0;
    //reg [31:0] block_address=32'd0;
    reg data_multiple=1'b0;
    reg data_last=1'b0;
    reg data_next=1'b0;
    /* outputs */
    wire [3:0] error;
    wire busy;
    wire idle;
    wire data_next_idle;
    wire version_2;
    wire card_hc;
    wire card_uhs;
    wire [119:0] card_csd;
    //wire [31:0] card_block_size;

    sd_controller sdc(.clk(clk), .reset(reset),
                      .sclk(sd_clk), .cmd(sd_cmd), .data(sd_dat), .card_detect(1'b1),
                      .ram_clk(sd_ram_clk), .ram_enable(sd_ram_enable),
                          .ram_write_enable(sd_ram_write_enable), .ram_address(sd_ram_address),
                          .ram_data_in(sd_ram_data_in), .ram_data_out(sd_ram_data_out),
                      .data_command_enable(data_enable), .write_block(data_write_block),
                          .erase_blocks(1'b0), .block_address(32'd0/*block_address*/),
                          .multiple_blocks(data_multiple), .fixed_count(1'b0),
                          .block_count(32'd0), .last_block(data_last),
                          .data_next(data_next),
                      .error(error), .idle(idle), .data_next_idle(data_next_idle),
                          .card_version_2(version_2), .card_hc(card_hc), .card_uhs(card_uhs),
                          .card_block_size());

    /* ram */
    reg ram_enable=1'b0;
    reg ram_write_enable=1'b0;
    reg [8:0] ram_address=9'd0;
    //reg [7:0] ram_data_in=8'd0;
    wire [7:0] ram_data_out;
    ram_dual_clock_dual_port #(.data_width(8), .address_width(9)) ram
                              (.A_clk(sd_ram_clk), .A_enable(sd_ram_enable),
                                   .A_write_enable(sd_ram_write_enable), .A_address(sd_ram_address),
                                   .A_data_in(sd_ram_data_in), .A_data_out(sd_ram_data_out),
                               .B_clk(clk), .B_enable(ram_enable), .B_write_enable(ram_write_enable),
                                   .B_address(ram_address),
                                   .B_data_in(/*ram_data_in*/), .B_data_out(ram_data_out));

    /* usb output */
    assign usb_siwua=1'b1;
    reg wr_en=1'b0;
    reg [7:0] wr_data;
    wire wr_full;

    ft245_async_fifo #(.read_depth(3), .write_depth(9), .same_clocks(1)) ft245
                      (.D(usb_data), .RXFn(usb_rxf), .TXEn(usb_txe), .RDn(usb_rd), .WRn(usb_wr),
                       .clk_50mhz(clk_50mhz), .rw_clk(clk),
                       .rd_en(1'b0), .rd_data(), .rd_empty(),
                       .wr_en(wr_en), .wr_data(wr_data), .wr_full(wr_full));

    localparam TEST_START=0, TEST_START_COMPLETE=1, TEST_READ_START=2, TEST_READ_WAIT=3,
               TEST_RAM_OUTPUT=4, TEST_READ_NEXT=5, TEST_END=6;
    reg [2:0] test_state=TEST_START;

    reg [13:0] send_counter='d0;
    reg trigger=1'b0;
    reg ram_toggle=1'b0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            test_state <= TEST_START;
            trigger <= 1'b0;
            send_counter <= 'd0;
        end
        else
        begin
            case(test_state)
            TEST_START:
            begin
                /* wait for sd_controller to idle */
                if(idle)
                begin
                    /* move to next state */
                    test_state <= TEST_START_COMPLETE;
                end
            end

            TEST_START_COMPLETE:
            begin
                if(!wr_full)
                begin
                    if(send_counter=='d0)
                    begin
                        /* 1 */
                        wr_data <= {error, idle, version_2, card_hc, card_uhs};
                        wr_en <= 1'b1;
                    end
                    else
                    begin
                        wr_en <= 1'b0;
                        /* move to next state */
                        if(error=='d0)
                            test_state <= TEST_READ_START;
                        else
                        begin
                            wr_data <= {4'd0, error};
                            wr_en <= 1'b1;
                            test_state <= TEST_END;
                        end
                    end

                    /* increment send counter */
                    send_counter <= send_counter + 1'b1;
                end
            end

            TEST_READ_START:
            begin
                /* reset ram */
                ram_enable <= 1'b0;
                ram_write_enable <= 1'b0;

                /* start read */
                data_enable <= 1'b1;
                data_write_block <= 1'b0;
                //block_address <= 32'd0;
                data_multiple <= 1'b1;
                data_last <= 1'b0;
                data_next <= 1'b0;

                /* move to next state */
                test_state <= TEST_READ_WAIT;
            end

            TEST_READ_WAIT:
            begin
                if(!idle && data_enable)
                    data_enable <= 1'b0;
                else if((data_next_idle||idle) && !data_enable)
                begin
                    if(idle)
                        /* TODO: raise error */
                        test_state <= TEST_END;
                    else
                    begin
                        send_counter <= 'd0;
                        ram_address <= 'd0;
                        ram_enable <= 1'b1;
                        ram_toggle <= 1'b0;
                        test_state <= TEST_READ_NEXT;
                    end
                end
            end

            TEST_RAM_OUTPUT:
            begin
                wr_en <= 1'b0;
                data_next <= 1'b0;

                if(!ram_toggle)
                begin
                    ram_enable <= 1'b1;
                    ram_address <= ram_address + 1'b1;
                    ram_toggle <= 1'b1;
                end
                else if(!wr_full)
                begin
                    wr_en <= 1'b1;
                    wr_data <= ram_data_out;
                    ram_toggle <= 1'b0;
                    if(ram_address==9'd0)
                    begin
                        if(send_counter=='d3)
                            test_state <= TEST_END;
                        else
                        begin
                            if(send_counter=='d2)
                            begin
                                trigger <= 1'b1;
                                data_last <= 1'b1;
                            end

                            test_state <= TEST_READ_NEXT;
                            data_next <= 1'b1;
                            send_counter <= send_counter +1'b1;
                        end
                    end
                end
            end

            TEST_READ_NEXT:
            begin
                ram_enable <= 1'b0;
                wr_en <= 1'b0;

                if(!data_next_idle && data_next)
                    data_next <= 1'b0;
                if(data_next_idle && !data_next)
                begin
                    ram_enable <= 1'b1;
                    ram_address <= 'd0;
                    test_state <= TEST_RAM_OUTPUT;
                end

                if(error!='d0)
                begin
                    wr_data <= {4'd0, error};
                    wr_en <= 1'b1;
                    test_state <= TEST_END;
                end
            end

            TEST_END:
            begin
                wr_en <= 1'b0;
            end
            endcase
        end
    end

    /* LEDs */
    assign LED1=reset; /* green hdmi */
    assign LED2=(error!='d0); /* red hdmi */
    assign LED3=card_hc; /* green sd */
    assign LED4=version_2; /* red sd */
    assign LED5=idle; /* red usb */

    /* logic analyser */
    assign Wing_C0=1'b0;
    assign Wing_C1=sd_clk;
    assign Wing_C2=sd_cmd;
    assign Wing_C3=sd_dat[0];
    assign Wing_C4=sd_dat[1];
    assign Wing_C5=sd_dat[2];
    assign Wing_C6=sd_dat[3];
    assign Wing_C7=trigger;

endmodule
