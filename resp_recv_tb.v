`timescale 1ns / 1ps

module resp_recv_tb;

	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [2:0] response_type;
	reg [19:0] timeout_len;
	reg cmd_line;

    wire clock_strobe;

	// Outputs
	wire sclk;
	wire busy;
	wire timeout_error;
    wire crc_error;
	wire [135:0] response;

	// Instantiate the Unit Under Test (UUT)
	resp_recv uut (
		.clk(clk),
		.reset(reset),
		.enable(enable),
		.response_type(response_type),
		.timeout_len(timeout_len),
		.clock_strobe(clock_strobe),
		.sclk(sclk),
		.cmd_line(cmd_line),
		.busy(busy),
		.timeout_error(timeout_error),
        .crc_error(crc_error),
		.response(response)
	);

    always #0.5 clk <= ~clk;

    mod_m_counter #(.m(2)) strobe(.clk(clk), .reset(1'b1), .enable(1'b1), .hit(clock_strobe));

    reg [47:0] cmd_output;

    /* cmd_line control */
    always @(posedge clk) if(sclk && clock_strobe) {cmd_line, cmd_output} <= {cmd_output, cmd_output[0]};

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		enable = 0;
		response_type = 0;
		timeout_len = 0;
		cmd_output = 48'hffffffffffff;

        #10;

        /* remove from reset */
        reset=1;
        #2;

        /* check timeout */
        response_type=1;
        timeout_len=10;
        enable=1;
        #1;
        enable=0;

        #100;

        if(!timeout_error)
        begin
            $display("FAIL - timeout_error not asserted");
            $finish;
        end

        /* check crc against a transmit message (not receive - this is command 0, with crc of 0x4a) */
        response_type=1;
        timeout_len=10;
        cmd_output = 48'hffffffffffff; /* set output high first */
        enable=1;
        #1;
        enable=0;
        #2;
        cmd_output={1'b0, 1'b1, 6'd0, 32'd0, 7'h4a, 1'b1}; /* after enable, load the data into the shift register */

        #250;

        if(timeout_error)
        begin
            $display("FAIL - crc transmit test failed - timeout_error asserted");
            $finish;
        end

        if(crc_error)
        begin
            $display("FAIL - crc transmit test failed - crc_error asserted");
            $finish;
        end

        /* check crc fails correctly */
        response_type=1;
        timeout_len=10;
        cmd_output = 48'hffffffffffff; /* set output high first */
        enable=1;
        #1;
        enable=0;
        #2;
        cmd_output={1'b0, 1'b0, 6'd0, 32'd0, 7'hff, 1'b1}; /* after enable, load the data into the shift register */

        #250;

        if(timeout_error)
        begin
            $display("FAIL - crc fail test failed - timeout_error asserted");
            $finish;
        end

        if(!crc_error)
        begin
            $display("FAIL - crc fail test failed - crc_error not asserted");
            $finish;
        end

        $display("PASS");
        $finish;

	end

endmodule
