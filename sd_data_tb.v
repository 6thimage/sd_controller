`timescale 1ns / 1ps

module sd_data_tb;

	// Inputs
	reg clk;
	reg reset;
	reg sclk;
	reg [3:0] data_in;
	reg [7:0] ram_read;
	reg enable;
	reg write_data;
	wire clock_strobe;

	// Outputs
	wire [3:0] data_out;
	wire data_enable;
	wire ram_clk;
	wire ram_enable;
	wire ram_write_enable;
	wire [8:0] ram_address;
	wire [7:0] ram_write;
	wire busy;
	wire response_busy;
    wire [2:0] error;

	// Instantiate the Unit Under Test (UUT)
	sd_data uut (
		.clk(clk),
		.reset(reset),
		.sclk(sclk),
		.data_in(data_in),
		.data_out(data_out),
		.data_enable(data_enable),
		.ram_clk(ram_clk),
		.ram_enable(ram_enable),
		.ram_write_enable(ram_write_enable),
		.ram_address(ram_address),
		.ram_read(ram_read),
		.ram_write(ram_write),
		.enable(enable),
		.write_data(write_data),
		.clock_strobe(clock_strobe),
		.busy(busy),
		.response_busy(response_busy),
        .error(error)
	);

    always #0.5 clk <= ~clk;

    /* clock strober */
    mod_m_counter #(.m(2)) strobe(.clk(clk), .reset(1'b1), .enable(1'b1), .hit(clock_strobe));

    /* correct behaviour for sclk - inverting at clock_strobe, controlled by sclk_enable in such a way
     * that if sclk_enable is set low during a clock period it will reset it back to one in the proper way
     */
    reg sclk_enable=0;
    always @(posedge clk)
    begin
        if(sclk_enable)
        begin
            if(clock_strobe)
                sclk <= ~sclk;
        end
        else
        begin
            if(!sclk)
            begin
                if(clock_strobe)
                    sclk <= 1'b1;
            end
            else
                sclk <= 1;
        end
    end

    /* data ram */
    reg [7:0] ram_data[511:0];

    always @(posedge ram_clk)
    begin
        if(ram_enable)
        begin
            ram_read <= ram_data[ram_address];
            if(ram_write_enable)
                ram_data[ram_address] <= ram_write;
        end
    end

    integer i;
    initial
    begin
        for(i=0; i<512; i=i+1)
            ram_data[i]=i;
    end

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		sclk = 0;
		data_in = 4'b1111;
		ram_read = 0;
		enable = 0;
		write_data = 0;

        #10;
		/* remove from reset */
        reset = 1;

        #2;
        /* enable data write */
        write_data = 1;
        enable = 1;

        #2;
        /* enable sclk */
        sclk_enable = 1;

        #2;
        /* disable enable */
        enable = 0;

        #4200;

        /* reset uut and ram */
        reset = 0;
        sclk_enable = 0;
        for(i=0; i<512; i=i+1)
            ram_data[i]='hx;
        #10;

        /* remove from reset */
        reset = 1;

        #2;
        /* enable data read */
        write_data = 0;
        enable = 1;
        sclk_enable = 1;

        #2;
        /* disable enable */
        enable = 0;

        #2;
        data_in = 4'b0000;

        #5000;
        $finish;

	end

endmodule

