`timescale 1ns / 1ps

module clk_strobe_tb;

	// Inputs
	reg clk;
	reg reset;
	reg [7:0] clk_div;

	// Outputs
	wire clk_strobe;

	// Instantiate the Unit Under Test (UUT)
	clk_strobe uut (
		.clk(clk),
		.reset(reset),
		.clk_div(clk_div),
		.clk_strobe(clk_strobe)
	);

    always #0.5 clk <= ~clk;

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		clk_div = 0;

        #5;

		/* no divider */
        clk_div=0;
        #1;
        reset=1;
        #20;

        reset=0;
        #5;

        /* divider of 1 */
        clk_div=1;
        #1;
        reset=1;
        #20;

        reset=0;
        #5;

        /* divider of 2 */
        clk_div=2;
        #1;
        reset=1;
        #20;

        reset=0;
        #5;

        /* divider of 255 */
        clk_div=255;
        #1;
        reset=1;
        #5000;

        reset=0;
        #5;

        $finish;
	end

endmodule

