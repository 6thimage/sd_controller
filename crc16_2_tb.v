`timescale 1ns / 1ps

module crc16_2_tb;

	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [1:0] data;

	// Outputs
	wire [15:0] crc;

	// Instantiate the Unit Under Test (UUT)
	crc16_2 uut (
		.clk(clk),
		.reset(reset),
		.enable(enable),
		.data(data),
		.crc(crc)
	);

    always #0.5 clk <= ~clk;

	initial begin
		// Initialize Inputs
		clk = 1;
		reset = 0;
		enable = 0;
		data = 0;

        /* test that 512 bytes of data results in crc of 0x7fa1 - defined in SD specification */
		#2;
        /* remove from reset */
        reset = 1;

        #2;
        /* send data */
        data=2'b11;
        enable=1;
        #2048; /* 512*4 - 2 bits added to crc each clock */

        /* disable enable */
        enable=0;
        #2;

        if(crc!=16'h7fa1)
        begin
            $display("FAIL - output crc is incorrect - expected 0x7fa1, got 0x%h", crc);
            $finish;
        end

        /* place in reset */
        #10;
        reset=0;
        #10;

        /* test that 128 bytes of data results in crc of 0x */
		#2;
        /* remove from reset */
        reset = 1;

        #2;
        /* send data */
        data=2'b11;
        enable=1;
        #512;

        /* disable enable */
        enable=0;
        #2;

        if(crc!=16'heda9)
        begin
            $display("FAIL - output crc is incorrect - expected 0xeda9, got 0x%h", crc);
            $finish;
        end

        $display("PASS");
        $finish;

	end

endmodule

